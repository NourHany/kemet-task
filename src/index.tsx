import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import App from "./App";
import { BrowserRouter } from "react-router-dom";
import ContextProvider from "./utils/Context/auth.context";
import axios from "axios";

axios.interceptors.request.use(
  (req: any) => {
    req.headers.token = localStorage.getItem("token");
    return req;
  },
  (err) => {
    return Promise.reject(err);
  }
);

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement
);
root.render(
  <React.StrictMode>
    <ContextProvider>
      <BrowserRouter>
        <App />
      </BrowserRouter>
    </ContextProvider>
  </React.StrictMode>
);
