import { useContext } from "react";
import { Routes, Route } from "react-router-dom";
import Home from "./pages/Home";
import Login from "./pages/Login";
import Register from "./pages/Register";

import { AuthContext } from "./utils/Context/auth.context";

const App = () => {
  const authCtx = useContext(AuthContext);

  return (
    <div className="App bg-gray-700 flex justify-center">
      <Routes>
        {authCtx.isLoggedIn && <Route path="/" element={<Home />} />}
        {!authCtx.isLoggedIn && (
          <>
            <Route path="register" element={<Register />} />
            <Route path="login" element={<Login />} />
          </>
        )}
      </Routes>
    </div>
  );
};

export default App;
