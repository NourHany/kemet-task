import axios from "axios";
import { RegisterUser, LoginUser } from "../types";

export const register = (user: RegisterUser) => {
  return axios.post(
    "https://berlin-club.herokuapp.com/api/auth/register",
    user
  );
};

export const login = (user: LoginUser) => {
  return axios.post("https://berlin-club.herokuapp.com/api/auth/login", user);
};
