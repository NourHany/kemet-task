import { createContext, FC, useEffect, useState, ReactNode } from "react";
import { RegisterUser } from "../../types";

type ContextProviderType = {
  children: ReactNode;
};

type AuthContextDataType = {
  userData?: RegisterUser;
  token: string | null;
  isLoggedIn: boolean;
  login: (token: string, userData: string) => void;
  logout: () => void;
};

let defaultAuthContextData: AuthContextDataType = {
  token: "",
  isLoggedIn: false,
  login: () => {},
  logout: () => {},
};

export const AuthContext = createContext(defaultAuthContextData);

export const ContextProvider: FC<ContextProviderType> = (props) => {
  const initToken = localStorage.getItem("token");
  const initUserData = JSON.parse(localStorage.getItem("userData")!);
  const [userData, setUserData] =
    useState<AuthContextDataType["userData"]>(initUserData);
  const [token, setToken] = useState<AuthContextDataType["token"]>(initToken);
  const userIsLoggedIn = !!token;

  const [authContextData, setAuthContextData] = useState<AuthContextDataType>(
    defaultAuthContextData
  );

  const loginHandler = (token: string, userData: any) => {
    setToken(token);
    setUserData(userData);
    localStorage.setItem("token", token);
    localStorage.setItem("userData", JSON.stringify(userData));
  };

  const logoutHandler = () => {
    setToken(null);
    localStorage.clear();
  };

  useEffect(() => {
    setAuthContextData({
      userData,
      token,
      isLoggedIn: userIsLoggedIn,
      login: loginHandler,
      logout: logoutHandler,
    });
  }, [userData, token, userIsLoggedIn]);

  return (
    <AuthContext.Provider value={authContextData}>
      {props.children}
    </AuthContext.Provider>
  );
};

export default ContextProvider;
