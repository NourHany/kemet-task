export type RegisterUser = {
  email: string;
  name: string;
  password: string;
};

export type LoginUser = {
  email: string;
  password: string;
};
